
public class Binario {
	private String binario;
	private int pos;
	private int aux;
	private int value;
	
	public Binario(String binario) {
		this.binario = binario;
		this.aux = 0;
		this.pos = 0;
		this.value = 0;
	}
	
	public int sol() 
	{
		for(int i = 0; i<binario.length()-1;i++) 
		{
			while(binario.charAt(i)=='1') 
			{
				aux++;
				i++;
			}
				if(value<aux) 
				{
					pos = i - aux;
					value = aux;
				}
				aux = 0;
		  }		
		return pos;
	}

}
